Commerce Canadian Taxes module for Drupal 7


DESCRIPTION
-----------

  Commerce Canadian Taxes builds on the Drupal Commerce's Commerce Tax module to
  calculate Canadian retails sales taxes. It deals with simple retail sales taxes
  and can be extended to support less common ones Canadian tax jurisdictions.

  Retail sales tax coverage is not complete, but will cover most situations.
  For example, the Ontario tax rule that exempts the provincial tax component
  for sales of food under $4 has not been implemented.

  The following retail sales taxes are supported:
    NL: HST, RST(vehicles)
    NS: HST
    PE: HST
    NB: HST, PVT, TPPT
    QC: GST, QST
    ON: HST, GST
    MB: GST, RST, PVT
    SK: GST, PST, LCT
    AB: GST
    BC: GST, PST
    YT: GST
    NT: GST
    NU: GST

  Legend:
    GST  = Goods and Services Tax
    HST  = Harmonized Sales Tax
    LCT  = Liquor Consumption Tax
    PST  = Provincial Sales Tax
    PVT  = Provincial Vehicle Tax
    QST  = Quebec Sales Tax
    RST  = Retail Sales Tax
    TPPT = Tangible Personal Property Tax


FEATURES
--------

  - Fields are created for each tax and grouped under each jurisdiction
  - A single field collection is added to each Commerce Product bundle
    and Commerce Shipping line item
  - Commerce Tax rates are defined for each tax in each jurisdiction
  - Taxes are configurable for each jurisdiction on a product level
  - Rules are used to calculate taxes on cart line items
  - A separate UI module is provided to initialize the tax field instances


LIMITATIONS
-----------

Default shipping tax settings: The exclusion of PST for MB, SK, and BC is handled in code for the time being.

Currently, there is one set of global defaults for all taxable things. However, line items are created transparently, meaning they don't exist until they're created for the cart. So, there is no easy user-configurable way to control what taxes are applicable on shipping without also affecting the default settings for products.

One approach would be to create a different set of defaults for each taxable entity.

Too bad shipping was not implemented as a product...


RELATIONSHIP TO OTHER MODULES
-----------------------------

  Commerce Canadian Taxes supports applying taxes to Commerce Line Items,
  including Commerce Shipping Line Items. The same tax field can be added
  to Shipping Line Items via the Commerce Canadian Taxes UI module.


UPGRADING FROM COMMERCE CANADIAN TAXES 7.x-1.x
----------------------------------------------

  Uninstalling Commerce Canadian Taxes 7.x-1.x

  1. Back up your installation. (Backing up a site - https://drupal.org/node/22281)

  2. Put your site in maintenance mode (Note #4 - https://drupal.org/node/570162)

  3. Disable 'Commerce Canadian Taxes'.

  4. Uninstall 'Commerce Canadian Taxes'.

  5. Delete the 'commerce_canadian_taxes folder'.

  6. Flush all caches, then take your site out of maintenance mode.

  7. If the tax rules from v7.x-1.x are not deleted following step #6,
     manually delete the following tax rules;
		'GST'
		'HST BC'
		'HST New Brunswick'
		'HST NL'
		'HST NS'
		'HST ON'
		'PST Manitoba'
		'PST PE'
		'PST SK'
		'PST Quebec (QST/TVQ)'


INSTALLATION
------------

  1. Download and install the Commerce Canadian Taxes module, which includes
     the Commerce Canadian Taxes UI module.

  2. Go to "Modules" (admin/modules) and enable both modules.

    - A field named 'Canadian Sales Taxes' (field_sales_taxes_ca) is
      automatically added to all Commerce Product types and Shipping Line Item
      (if installed) with default settings for applicable taxes.

  3. At this point, the tax fields are created but existing host instances are
     not yet initialized. In this case, the host instances are the various
     commerce_product bundles.

     Normally, you would use Views Bulk Operations (VBO) to initialize your
     products. However, initialization of Field Collections does not work
     with VBO.

     The Commerce Canadian Taxes UI module will initialize all product
     instances in user-configurable batch sizes. Depending on the performance
     of your server, large batch sizes (> 25 products per batch) may time-out.

     Alternatively, you can load and save each existing product one-by-one
     using the Commerce Product UI.

  4. Change the applicable tax settings for each product, as necessary.


  Module Dependencies:
    Drupal core 7.x
    Commerce Line Item
    Commerce Order
    Commerce Tax
    Commerce Tax UI
    Entity
    Field
    Field Collection
    List
    Options
    Rules


CONFIGURATION
-------------

  Tax rates are configured using Commerce Tax Rate UI:
    "Store > Configuration > Taxes"
    .../admin/commerce/config/taxes

  Rules are configured using the Rules UI:
    "Configuration > Workflow > Rules > Components"
    .../admin/config/workflow/rules/components

  Default applicable tax settings are configured using the Field Collections
  UI:
    "Structure > Field collections"
    .../admin/structure/field-collections
    - To edit the default tax field settings, click "manage fields" for each
      Administrative Area (province), and edit the default value of each tax
      field (boolean).

  Applicable taxes can be set for each product for each product using the
  Commerce Product UI:
    "Store > Products"
    .../admin/commerce/products
    - Simply open the product and update the tax settings under "Canadian Sales
      Taxes".


INTEGRATION WITH OTHER COMMERCE MODULES
---------------------------------------

  Commerce Canadian Taxes 7.x-2.x integrates with Commerce Shipping (7.x-2.0) but needs
  to be configured to function properly.

	1. Change the 'Apply sales tax to shipping' rule to use the new 'Canadian sales taxes'.

			a) Navigate to rules configuration (Configuration > Workflow > Rules)
			b) Edit rule 'Apply sales tax to shipping'
			c) Edit action 'Calculate taxes for a line item'
			d) Change 'Tax type'
				From:[Sales tax]
				To:[Canadian sales taxes]
			e) Save the rule.

	2. Add the existing field 'Canadian Sales Taxes' (field_sales_taxes_ca) to
		 the shipping line item.

			a) Navigate to the Shipping Line Item fields
			   (Store > Configuration > Line Item Types > Shipping > Manage fields).
			b) Click on '-Select an existing field-'.
			c) Choose 'Canadian Sales Taxes' (or 'Field collection: field_sales_taxes_ca
			   (Canadian Sales Taxes)' as it appears in the select.
			d) Change the 'Widget Type' from 'Embedded' to 'Hidden' (optional).
			e) Save the Shipping line item fields.

  Note: Commerce Canadian Taxes integrates with Commerce Shipping using hook
        'hook_commerce_shipping_line_item_new_alter' to initialize the line
        item's tax field.
  
        If you need to charge taxes on other line item types you will need 
        to implement the equivalent hook, if it exists.


RELATED ISSUES
--------------

  1. Issue https://drupal.org/node/2029043 related to the latest release of Internationalization
     - Need to patch to get Boolean field labels to show instead of "On Value"

  2. Can't update Field Collections with Views Bulk Operations (VBO)


SUPPORT
-------

  Project page: https://drupal.org/project/commerce_canadian_taxes
  Project issue queue: https://drupal.org/project/issues/commerce_canadian_taxes

  Sponsored by:
    Esper Consulting Inc., Ottawa, Canada
    http://www.esper.ca

  Maintainers:
    - George Esper:   https://drupal.org/user/725082
    - Antony Lovric:  https://drupal.org/user/1302300
    - Omar Touma:     https://drupal.org/user/1807142
    - Boris Gontar    https://drupal.org/user/2619559
