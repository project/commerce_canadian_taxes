<?php
/**
 * @file
 * commerce_canadian_taxes.inc
 */

/**
 * Implements hook_commerce_tax_type_info().
 *
 * Defines tax types used to categorize tax rates.
 *
 * Tax References:
 * @see: http://www.canadabusiness.ca/eng/page/2651
 * @see: http://en.wikipedia.org/wiki/Sales_taxes_in_Canada
 *
 */
function commerce_canadian_taxes_commerce_tax_type_info() {

  $tax_types = array();

  $tax_types['sales_tax_canada'] = array(
    'title' => t('Canadian sales taxes'),
    'display_title' => t('Canadian sales taxes'),
    'description' => t('Commerce tax type for Canadian sales taxes.'),
    'display_inclusive' => FALSE,
    'round_mode' => COMMERCE_ROUND_NONE,
    'rule' => 'commerce_tax_type_sales_tax_canada',
    'admin_list' => TRUE,
    'weight' => -2,
  );

  return $tax_types;
}

/**
 * Implements hook_commerce_tax_rate_info().
 *
 * Defines tax rates that may be applied to line items.
 *
 * Added "display_weight" and "rule_weight" to the return array. They are used
 * to order price components (Subtotal, taxes, etc.) in the shopping cart.
 *
 * Currently, Component weights are Subtotal = -50; Total = 1
 *
 */
function commerce_canadian_taxes_commerce_tax_rate_info() {
  $tax_rate = array();

  // GST - Canada
  $tax_rate['ca_gst'] = array(
    'name' => 'ca_gst',
    'title' => t('CA GST'),
    'display_title' => t('GST'),
    'description' => t('Canadian Goods and Services Tax'),
    'rate' => 0.05,
    'type' => 'sales_tax_canada',
    'rules_component' => 'commerce_tax_rate_calculate_ca_gst',
    'default_rules_component' => FALSE,
    'price_component' => 'tax|ca_gst',
    'calculation_callback' => 'commerce_tax_rate_calculate',
    'module' => 'commerce_tax_ui',
    'admin_list' => TRUE,
    'display_weight' => -6,
    'rule_weight' => 0,
  );

  // Newfoundland and Labrador
  $tax_rate['ca_nl_hst'] = array(
    'name' => 'ca_nl_hst',
    'title' => t('HST NL'),
    'display_title' => t('HST NL'),
    'description' => t('Newfoundland and Labrador HST'),
    'rate' => 0.13,
    'type' => 'sales_tax_canada',
    'rules_component' => 'commerce_tax_rate_calculate_ca_nl_hst',
    'default_rules_component' => FALSE,
    'price_component' => 'tax|ca_nl_hst',
    'calculation_callback' => 'commerce_tax_rate_calculate',
    'module' => 'commerce_tax_ui',
    'admin_list' => TRUE,
    'display_weight' => -5,
    'rule_weight' => 0,
  );
  $tax_rate['ca_nl_pvt'] = array(
    'name' => 'ca_nl_pvt',
    'title' => t('RST NL'),
    'display_title' => t('RST NL'),
    'description' => t('Newfoundland and Labrador RST'),
    'rate' => 0.14,
    'type' => 'sales_tax_canada',
    'rules_component' => 'commerce_tax_rate_calculate_ca_nl_pvt',
    'default_rules_component' => FALSE,
    'price_component' => 'tax|ca_nl_pvt',
    'calculation_callback' => 'commerce_tax_rate_calculate',
    'module' => 'commerce_tax_ui',
    'admin_list' => TRUE,
    'display_weight' => -2,
    'rule_weight' => 0,
  );

  // Nova Scotia
  $tax_rate['ca_ns_hst'] = array(
    'name' => 'ca_ns_hst',
    'title' => t('HST NS'),
    'display_title' => t('HST NS'),
    'description' => t('Nova Scotia HST'),
    'rate' => 0.15,
    'type' => 'sales_tax_canada',
    'rules_component' => 'commerce_tax_rate_calculate_ca_ns_hst',
    'default_rules_component' => FALSE,
    'price_component' => 'tax|ca_ns_hst',
    'calculation_callback' => 'commerce_tax_rate_calculate',
    'module' => 'commerce_tax_ui',
    'admin_list' => TRUE,
    'display_weight' => -5,
    'rule_weight' => 0,
  );

  // Prince Edward Island
  $tax_rate['ca_pe_hst'] = array(
    'name' => 'ca_pe_hst',
    'title' => t('HST PE'),
    'display_title' => t('HST PE'),
    'description' => t('Prince Edward Island HST'),
    'rate' => 0.14,
    'type' => 'sales_tax_canada',
    'rules_component' => 'commerce_tax_rate_calculate_ca_pe_hst',
    'default_rules_component' => FALSE,
    'price_component' => 'tax|ca_pe_hst',
    'calculation_callback' => 'commerce_tax_rate_calculate',
    'module' => 'commerce_tax_ui',
    'admin_list' => TRUE,
    'display_weight' => -5,
    'rule_weight' => 0,
  );

  // New Brunswick - HST
  $tax_rate['ca_nb_hst'] = array(
    'name' => 'ca_nb_hst',
    'title' => t('HST NB'),
    'display_title' => t('HST NB'),
    'description' => t('New Brunswick HST'),
    'rate' => 0.13,
    'type' => 'sales_tax_canada',
    'rules_component' => 'commerce_tax_rate_calculate_ca_nb_hst',
    'default_rules_component' => FALSE,
    'price_component' => 'tax|ca_nb_hst',
    'calculation_callback' => 'commerce_tax_rate_calculate',
    'module' => 'commerce_tax_ui',
    'admin_list' => TRUE,
    'display_weight' => -5,
    'rule_weight' => 0,
  );

  // New Brunswick - PVT - Provincial Vehicle Tax
  // (motor vehicle purchased through a private sale)
  $tax_rate['ca_nb_pvt'] = array(
    'name' => 'ca_nb_pvt',
    'title' => t('PVT NB'),
    'display_title' => t('PVT NB'),
    'description' => t('New Brunswick PVT - Provincial Vehicle Tax'),
    'rate' => 0.13,
    'type' => 'sales_tax_canada',
    'rules_component' => 'commerce_tax_rate_calculate_ca_nb_pvt',
    'default_rules_component' => FALSE,
    'price_component' => 'tax|ca_nb_pvt',
    'calculation_callback' => 'commerce_tax_rate_calculate',
    'module' => 'commerce_tax_ui',
    'admin_list' => TRUE,
    'display_weight' => -2,
    'rule_weight' => 0,
  );

  // New Brunswick - TPPT - Tangible Personal Property Tax
  // (boat and or aircraft purchased through a private sale)
  $tax_rate['ca_nb_tppt'] = array(
    'name' => 'ca_nb_tppt',
    'title' => t('TPPT NB'),
    'display_title' => t('TPPT NB'),
    'description' => t('New Brunswick TPPT - Tangible Personal Property Tax'),
    'rate' => 0.13,
    'type' => 'sales_tax_canada',
    'rules_component' => 'commerce_tax_rate_calculate_ca_nb_tppt',
    'default_rules_component' => FALSE,
    'price_component' => 'tax|ca_nb_tppt',
    'calculation_callback' => 'commerce_tax_rate_calculate',
    'module' => 'commerce_tax_ui',
    'admin_list' => TRUE,
    'display_weight' => 0,
    'rule_weight' => 0,
  );

  // Quebec
  $tax_rate['ca_qc_pst'] = array(
    'name' => 'ca_qc_pst',
    'title' => t('QST QC'),
    'display_title' => t('QST'),
    'description' => t('Quebec QST'),
    'rate' => 0.09975,
    'type' => 'sales_tax_canada',
    'rules_component' => 'commerce_tax_rate_calculate_ca_qc_pst',
    'default_rules_component' => FALSE,
    'price_component' => 'tax|ca_qc_pst',
    'calculation_callback' => 'commerce_tax_rate_calculate',
    'module' => 'commerce_tax_ui',
    'admin_list' => TRUE,
    'display_weight' => -4,
    'rule_weight' => 0,
  );

  // Ontario
  $tax_rate['ca_on_hst'] = array(
    'name' => 'ca_on_hst',
    'title' => t('HST ON'),
    'display_title' => t('HST ON'),
    'description' => t('Ontario HST'),
    'rate' => 0.13,
    'type' => 'sales_tax_canada',
    'rules_component' => 'commerce_tax_rate_calculate_ca_on_hst',
    'default_rules_component' => FALSE,
    'price_component' => 'tax|ca_on_hst',
    'calculation_callback' => 'commerce_tax_rate_calculate',
    'module' => 'commerce_tax_ui',
    'admin_list' => TRUE,
    'display_weight' => -5,
    'rule_weight' => 0,
  );

  // Manitoba
  $tax_rate['ca_mb_pst'] = array(
    'name' => 'ca_mb_pst',
    'title' => t('RST MB'),
    'display_title' => t('RST MB'),
    'description' => t('Manitoba RST'),
    'rate' => 0.08, // 0.07 -> 0.08 as of 2013.07.01
    'type' => 'sales_tax_canada',
    'rules_component' => 'commerce_tax_rate_calculate_ca_mb_pst',
    'default_rules_component' => FALSE,
    'price_component' => 'tax|ca_mb_pst',
    'calculation_callback' => 'commerce_tax_rate_calculate',
    'module' => 'commerce_tax_ui',
    'admin_list' => TRUE,
    'display_weight' => -4,
    'rule_weight' => 0,
  );

  // Saskatchewan
  $tax_rate['ca_sk_pst'] = array(
    'name' => 'ca_sk_pst',
    'title' => t('PST SK'),
    'display_title' => t('PST SK'),
    'description' => t('Saskatchewan PST'),
    'rate' => 0.05,
    'type' => 'sales_tax_canada',
    'rules_component' => 'commerce_tax_rate_calculate_ca_sk_pst',
    'default_rules_component' => FALSE,
    'price_component' => 'tax|ca_sk_pst',
    'calculation_callback' => 'commerce_tax_rate_calculate',
    'module' => 'commerce_tax_ui',
    'admin_list' => TRUE,
    'display_weight' => -4,
    'rule_weight' => 0,
  );

  // Saskatchewan - Liquor tax
  $tax_rate['ca_sk_plt'] = array(
    'name' => 'ca_sk_plt',
    'title' => t('LCT SK'),
    'display_title' => t('LCT SK'),
    'description' => t('Saskatchewan LCT - Liquor Consumption Tax'),
    'rate' => 0.10,
    'type' => 'sales_tax_canada',
    'rules_component' => 'commerce_tax_rate_calculate_ca_sk_plt',
    'default_rules_component' => FALSE,
    'price_component' => 'tax|ca_sk_plt',
    'calculation_callback' => 'commerce_tax_rate_calculate',
    'module' => 'commerce_tax_ui',
    'admin_list' => TRUE,
    'display_weight' => -1,
    'rule_weight' => 0,
  );

  // British Columbia
  $tax_rate['ca_bc_pst'] = array(
    'name' => 'ca_bc_pst',
    'title' => t('PST BC'),
    'display_title' => t('PST BC'),
    'description' => t('British Columbia PST'),
    'rate' => 0.07,
    'type' => 'sales_tax_canada',
    'rules_component' => 'commerce_tax_rate_calculate_ca_bc_pst',
    'default_rules_component' => FALSE,
    'price_component' => 'tax|ca_bc_pst',
    'calculation_callback' => 'commerce_tax_rate_calculate',
    'module' => 'commerce_tax_ui',
    'admin_list' => TRUE,
    'display_weight' => -4,
    'rule_weight' => 0,
  );

  return $tax_rate;
}
